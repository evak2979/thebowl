﻿using CMT.Business.Concretes;
using NUnit.Framework;

namespace CTM.Integration
{
    [TestFixture]
    public class CtmEndToEndTests
    {
        private ScoreCalculator _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new ScoreCalculator(new ScoreDtoValidator(), new ScoreDtoBuilder(), 
                new DecoratedFrameFactory(new RollsFactory()));
        }

        [Test]
        public void should_return_300_for_all_strikes()
        {
            //given
            string allStrikes = "X|X|X|X|X|X|X|X|X|X||XX";

            //when
            int score = _sut.CalculateScore(allStrikes);

            //then
            Assert.That(score, Is.EqualTo(300));
        }

        [Test]
        public void should_return_90_for_all_nine()
        {
            //given
            string allStrikes = "9-|9-|9-|9-|9-|9-|9-|9-|9-|9-||";

            //when
            int score = _sut.CalculateScore(allStrikes);

            //then
            Assert.That(score, Is.EqualTo(90));
        }

        [Test]
        public void should_return_150_for_all_spares()
        {
            //given
            string allStrikes = "5/|5/|5/|5/|5/|5/|5/|5/|5/|5/||5";

            //when
            int score = _sut.CalculateScore(allStrikes);

            //then
            Assert.That(score, Is.EqualTo(150));
        }

        [Test]
        public void should_return_167_for_exercise_file_last_string()
        {
            //given
            string allStrikes = "X|7/|9-|X|-8|8/|-6|X|X|X||81";

            //when
            int score = _sut.CalculateScore(allStrikes);

            //then
            Assert.That(score, Is.EqualTo(167));
        }
    }
}
