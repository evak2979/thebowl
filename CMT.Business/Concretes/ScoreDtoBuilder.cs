﻿using System;
using CMT.Business.Interfaces;
using CMT.Domain;

namespace CMT.Business.Concretes
{
    public class ScoreDtoBuilder : IScoreDtoBuilder
    {
        public ScoreDto GetScoreDto(string scoreString)
        {
            ValidateNotEmptyString(scoreString);

            ScoreDto scoreDto = CreateScoreDto(scoreString);
            return scoreDto;
        }

        private void ValidateNotEmptyString(string scoreString)
        {
            if (string.IsNullOrEmpty(scoreString))
                throw new ArgumentException("Score string cannot be null or empty.");
        }

        private ScoreDto CreateScoreDto(string scoreString)
        {
            string[] splitScoreString = GetSplitScoreString(scoreString);
            ValidateScoreStringArray(splitScoreString);

            return new ScoreDto()
            {
                Frames = GetFramesStringArray(splitScoreString[0]),
                BonusRolls = splitScoreString[1]
            };
        }

        private void ValidateScoreStringArray(string[] splitScoreString)
        {
            if (splitScoreString.Length != 2)
            {
                throw new ArgumentException("Invalid score string");
            }
        }

        private string[] GetSplitScoreString(string scoreString)
        {
            string[] splitScoreString = GetScoreStringArray(scoreString);

            return splitScoreString;
        }

        private static string[] GetScoreStringArray(string scoreString)
        {
            return scoreString.Split(new[] {"||"}, StringSplitOptions.None);
        }
        

        private string[] GetFramesStringArray(string scoreString)
        {
            return scoreString.Split(new [] {"|"}, StringSplitOptions.None);
        }
    }
}
