using CMT.Business.Interfaces;

namespace CMT.Business.Concretes
{
    public class BonusFrame : Frame
    {
        public BonusFrame(IFrame nextFrame) : base(nextFrame)
        {
            
        }
        
        public override int GetScore()
        {
            return 0;
        }

        public override int GetFirstRoll()
        {
            return FirstRoll;
        }

        public override int GetSecondRoll()
        {
            return SecondRoll;
        }
    }
}