using System;
using System.Linq;
using System.Text.RegularExpressions;
using CMT.Business.Interfaces;
using CMT.Domain;

namespace CMT.Business.Concretes
{
    public class ScoreDtoValidator : IScoreDtoValidator
    {
        private readonly Regex _frameRegex = new Regex(@"^(([Xx]{1,1})|([1-9\-/]{2,2}))$");
        private readonly Regex _bonusRegex = new Regex(@"^(([Xx]{0,2})|([1-9]{0,2}))$");

        public void ValidateScoreDto(ScoreDto scoreDto)
        {
            ValidateBonusRolls(scoreDto.BonusRolls);
            ValidateFramesString(scoreDto.Frames);
        }

        private void ValidateBonusRolls(string bonusRolls)
        {
            if (!string.IsNullOrEmpty(bonusRolls))
            {
                if (!_bonusRegex.IsMatch(bonusRolls))
                {
                    throw new ArgumentException("Invalid bonus rolls value");
                }
            }
        }

        private void ValidateFramesString(string[] framesStringArray)
        {
            CheckFramesStringLength(framesStringArray);

            for (int i = 1; i <= framesStringArray.Length; i++)
            {
                ValidateFrame(framesStringArray[i - 1], i);
            }
        }

        private void CheckFramesStringLength(string[] framesStringArray)
        {
            if (framesStringArray.Length != 10)
            {
                throw new ArgumentException($"There should be ten frames in the score string. Number of frames found: {framesStringArray.Length}");
            }
        }

        private void ValidateFrame(string frameString, int index)
        {
            ValidateFrameFormat(frameString, index);
            ValidateFrameSum(frameString, index);
        }

        private void ValidateFrameSum(string frameString, int index)
        {
            if (!IsStrike(frameString) && !IsSpare(frameString) && !HasMissedRoll(frameString))
            {
                string[] frameStringArray = frameString.Select(x => x.ToString()).ToArray();
                int firstRoll = int.Parse(frameStringArray[0]);
                int secondRoll = int.Parse(frameStringArray[1]);

                if (firstRoll + secondRoll > 10)
                {
                    throw new ArgumentException($"Frame score cannot be higher than ten. Frame: {index}");
                }
            }
        }

        private bool HasMissedRoll(string frameString)
        {
            return frameString.IndexOf("-") > -1;
        }

        private bool IsSpare(string frameString)
        {
            return frameString.IndexOf("/") > -1;
        }

        private static bool IsStrike(string frameString)
        {
            return frameString.Length == 1;
        }

        private void ValidateFrameFormat(string frameString, int index)
        {
            if (!_frameRegex.IsMatch(frameString))
            {
                throw new ArgumentException($"Frame number {index} of the scorestring is erroneous.");
            }
        }
    }
}