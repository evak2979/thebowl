﻿using CMT.Business.Interfaces;
using CMT.Domain;

namespace CMT.Business.Concretes
{
    public interface IFrameFactory
    {
        IFrame GetFrame(ScoreDto scoreDto);
    }
}