﻿using System.Collections.Generic;
using System.Linq;
using CMT.Business.Interfaces;
using CMT.Domain;

namespace CMT.Business.Concretes
{
    public class DecoratedFrameFactory : IFrameFactory
    {
        private readonly IRollsFactory _rollsFactory;
        private IFrame _frame;
        private readonly Stack<IFrame> _frameStack = new Stack<IFrame>();

        public DecoratedFrameFactory(IRollsFactory rollsFactory)
        {
            _rollsFactory = rollsFactory;
        }

        public IFrame GetFrame(ScoreDto scoreDto)
        {
            ReverseScoreDtoFramesString(scoreDto);
            _frame = GetBonusRollFrame(scoreDto.BonusRolls);
            AddLatestFrameToStack(_frame);

            foreach (string frame in scoreDto.Frames)
            {
                DecorateFrame(frame);
            }

            return GetFinalFrame();
        }

        private IFrame GetBonusRollFrame(string bonusFrame)
        {
            Rolls rolls = _rollsFactory.GetRolls(bonusFrame);
            return CreateBonusFrame(rolls);
        }

        private static IFrame CreateBonusFrame(Rolls rolls)
        {
            return new BonusFrame(null)
            {
                FirstRoll = rolls.FirstRoll,
                SecondRoll = rolls.SecondRoll
            };
        }

        private void DecorateFrame(string frame)
        {
            Rolls rolls = _rollsFactory.GetRolls(frame);

            if (rolls.IsStrike)
            {
                GetStrikeFrame(rolls);
            }
            if (rolls.IsSpare)
            {
                GetSpareFrame(rolls);
            }
            if (rolls.IsNormal)
            {
                GetNormalFrame(rolls);
            }
        }

        private void GetStrikeFrame(Rolls rolls)
        {
            _frame = CreateStrikeFrame(rolls);
            AddLatestFrameToStack(_frame);
        }

        private StrikeFrame CreateStrikeFrame(Rolls rolls)
        {
            return new StrikeFrame(GetLastFrameFromStack())
            {
                FirstRoll = rolls.FirstRoll,
                SecondRoll = rolls.SecondRoll
            };
        }

        private void GetSpareFrame(Rolls rolls)
        {
            _frame = CreateSpareFrame(rolls);
            AddLatestFrameToStack(_frame);
        }

        private SpareFrame CreateSpareFrame(Rolls rolls)
        {
            return new SpareFrame(GetLastFrameFromStack())
            {
                FirstRoll = rolls.FirstRoll,
                SecondRoll = rolls.SecondRoll
            };
        }

        private void GetNormalFrame(Rolls rolls)
        {
            CreateNormalFrame(rolls);
            AddLatestFrameToStack(_frame);
        }

        private void CreateNormalFrame(Rolls rolls)
        {
            _frame = new Frame(GetLastFrameFromStack())
            {
                FirstRoll = rolls.FirstRoll,
                SecondRoll = rolls.SecondRoll
            };
        }

        private IFrame GetLastFrameFromStack()
        {
            return _frameStack.Pop();
        }

        private void AddLatestFrameToStack(IFrame frame)
        {
            _frameStack.Push(frame);
        }

        private IFrame GetFinalFrame()
        {
            return _frameStack.Pop();
        }

        private void ReverseScoreDtoFramesString(ScoreDto scoreDto)
        {
            scoreDto.Frames = scoreDto.Frames.Reverse().ToArray();
        }
    }
}