﻿using CMT.Business.Interfaces;
using CMT.Domain;

namespace CMT.Business.Concretes
{
    public class ScoreCalculator : IScoreCalculator
    {
        private readonly IScoreDtoValidator _scoreDtoValidator;
        private readonly IScoreDtoBuilder _scoreDtoBuilder;
        private readonly IFrameFactory _frameFactory;

        public ScoreCalculator(IScoreDtoValidator scoreDtoValidator, IScoreDtoBuilder scoreDtoBuilder,
            IFrameFactory frameFactory)
        {
            _scoreDtoValidator = scoreDtoValidator;
            _scoreDtoBuilder = scoreDtoBuilder;
            _frameFactory = frameFactory;
        }

        public int CalculateScore(string scoreString)
        {
            ScoreDto scoreDto = _scoreDtoBuilder.GetScoreDto(scoreString);
            _scoreDtoValidator.ValidateScoreDto(scoreDto);
            IFrame frame = _frameFactory.GetFrame(scoreDto);

            return frame.GetScore();
        }
    }
}