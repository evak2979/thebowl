﻿using CMT.Business.Interfaces;

namespace CMT.Business.Concretes
{
    public class StrikeFrame : Frame
    {
        public StrikeFrame(IFrame nextFrame) : base(nextFrame)
        {
        }

        public override int GetSecondRoll()
        {
            return nextFrame.GetFirstRoll();
        }

        public override int GetScore()
        {
            return base.GetScore() + nextFrame.GetFirstRoll() + nextFrame.GetSecondRoll();
        }
    }
}
