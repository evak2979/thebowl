﻿namespace CMT.Business.Concretes
{
    public class Rolls
    {
        public int FirstRoll { get; set; }
        public int SecondRoll { get; set; }
        public bool IsStrike => FirstRoll == 10;
        public bool IsSpare => FirstRoll > 0 && SecondRoll > 0 && FirstRoll + SecondRoll == 10;
        public bool IsNormal => FirstRoll + SecondRoll < 10;
    }
}