﻿using CMT.Business.Interfaces;

namespace CMT.Business.Concretes
{
    public class SpareFrame : Frame
    {
        public SpareFrame(IFrame nextFrame) : base(nextFrame)
        {
        }

        public override int GetScore()
        {
            return base.GetScore() + nextFrame.GetFirstRoll();
        }
    }
}
