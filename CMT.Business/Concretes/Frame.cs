using CMT.Business.Interfaces;

namespace CMT.Business.Concretes
{
    public class Frame : IFrame
    {
        public int FirstRoll { protected get; set; }
        public int SecondRoll { protected get; set; }

        protected readonly IFrame nextFrame;

        public Frame(IFrame nextFrame)
        {
            this.nextFrame = nextFrame;
        }
        
        public virtual int GetScore()
        {
            return FirstRoll + SecondRoll + nextFrame.GetScore();
        }

        public virtual int GetFirstRoll()
        {
            return FirstRoll;
        }

        public virtual int GetSecondRoll()
        {
            return SecondRoll;
        }
    }
}