﻿using System;
using System.Linq;
using CMT.Business.Interfaces;

namespace CMT.Business.Concretes
{
    public class RollsFactory : IRollsFactory
    {
        public Rolls GetRolls(string frame)
        {
            string[] frameParts = GetFrameParts(frame);
            return CreateRoll(frameParts);
        }

        private Rolls CreateRoll(string[] frameParts)
        {
            switch (frameParts.Length)
            {
                case 0:
                    return GetZeroRolls();
                case 1:
                    if (IsStrike(frameParts))
                    {
                        return GetStrikeRoll();
                    }
                    else
                    {
                        return GetSingleBonusRoll(frameParts);
                    }
                    
                default:
                    return GetNormalRoll(frameParts);
            }
        }

        private Rolls GetZeroRolls()
        {
            return new Rolls();
        }

        private bool IsStrike(string[] frameParts)
        {
            return frameParts[0].Equals("X", StringComparison.OrdinalIgnoreCase);
        }

        private Rolls GetStrikeRoll()
        {
            return new Rolls
            {
                FirstRoll = 10,
                SecondRoll = 0
            };
        }

        private Rolls GetSingleBonusRoll(string[] frameParts)
        {
            return new Rolls
            {
                FirstRoll = int.Parse(frameParts[0])
            };
        }

        private Rolls GetNormalRoll(string[] frameParts)
        {
            return new Rolls
            {
                FirstRoll = GetRollValue(frameParts[0], frameParts),
                SecondRoll = GetRollValue(frameParts[1], frameParts)
            };
        }

        private int GetRollValue(string framePart, string[] frameParts)
        {
            switch (framePart)
            {
                case "X":
                case "x":
                    return 10;
                case "-":
                    return 0;
                case "/":
                    return GetRemainingValue(frameParts);
                default:
                    return int.Parse(framePart);
            }
        }

        private int GetRemainingValue(string[] frameParts)
        {
            int spareRemainder = int.Parse(frameParts.First(x => x != "/"));
            return 10 - spareRemainder;
        }
        private string[] GetFrameParts(string frame)
        {
            return frame.ToCharArray().Select(c => c.ToString()).ToArray();
        }
    }
}