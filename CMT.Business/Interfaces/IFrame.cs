namespace CMT.Business.Interfaces
{
    public interface IFrame
    {
        int GetScore();
        int GetFirstRoll();
        int GetSecondRoll();
    }
}