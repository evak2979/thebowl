﻿using CMT.Domain;

namespace CMT.Business.Interfaces
{
    public interface IScoreDtoValidator
    {
        void ValidateScoreDto(ScoreDto dto);
    }
}
