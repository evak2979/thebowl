﻿using CMT.Business.Concretes;

namespace CMT.Business.Interfaces
{
    public interface IRollsFactory
    {
        Rolls GetRolls(string frame);
    }
}