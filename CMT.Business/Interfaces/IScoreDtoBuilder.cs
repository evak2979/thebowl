﻿using CMT.Domain;

namespace CMT.Business.Interfaces
{
    public interface IFrameValidator
    {
        void Validate(IFrame frame);
    }

    public interface IScoreDtoBuilder
    {
        ScoreDto GetScoreDto(string scoreString);
    }
}
