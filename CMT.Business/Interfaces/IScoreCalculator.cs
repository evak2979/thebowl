﻿

namespace CMT.Business.Interfaces
{
    public interface IScoreCalculator
    {
        int CalculateScore(string scoreString);
    }
}