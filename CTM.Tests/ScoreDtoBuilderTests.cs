﻿using System;
using System.Linq;
using CMT.Business.Concretes;
using CMT.Domain;
using NUnit.Framework;

namespace CTM.Unit
{
    [TestFixture]
    public class ScoreDtoBuilderTests
    {
        private ScoreDtoBuilder _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new ScoreDtoBuilder();
        }

        [TestCase(null)]
        [TestCase("")]
        public void should_throw_argument_exception_for_null_or_empty_scorestring(string scoreString)
        {
            //when
            Assert.Throws<ArgumentException>(() => _sut.GetScoreDto(scoreString));
        }

        [Test]
        public void should_throw_exception_if_scorestring_cannot_be_split_in_two_parts()
        {
            //given
            string scoreString = "abc";

            //when
            Assert.Throws<ArgumentException>(() => _sut.GetScoreDto(scoreString));
        }

        [Test]
        public void should_properly_populate_scoredto_frames_with_thereverse_the_frames_string()
        {
            //given
            string scoreString = "abc|def||cd";

            //when
            ScoreDto scoreDto = _sut.GetScoreDto(scoreString);

            //then
            Assert.That(scoreDto.Frames.First(), Is.EqualTo("abc"));
            Assert.That(scoreDto.Frames.Skip(1).First(), Is.EqualTo("def"));
        }

        [Test]
        public void should_properly_populate_scoredto_bonus()
        {
            //given
            string scoreString = "abc||cd";

            //when
            ScoreDto scoreDto = _sut.GetScoreDto(scoreString);

            //then
            Assert.That(scoreDto.BonusRolls, Is.EqualTo("cd"));
        }
    }
}
