﻿using CMT.Business.Concretes;
using CMT.Business.Interfaces;
using Moq;
using NUnit.Framework;

namespace CTM.Unit
{
    public class StrikeFrameTests
    {
        private StrikeFrame _sut;
        private Mock<IFrame> _mockFrame;

        [SetUp]
        public void Setup()
        {
            _mockFrame = new Mock<IFrame>();
            _sut = new StrikeFrame(_mockFrame.Object);
        }

        [Test]
        public void should_return_first_roll_correctly()
        {
            //given
            _sut.FirstRoll = 1;

            //when
            int firstRoll = _sut.GetFirstRoll();

            //then
            Assert.That(firstRoll, Is.EqualTo(1));
        }

        [Test]
        public void should_return_second_roll_correctly()
        {
            //given
            _mockFrame.Setup(x => x.GetFirstRoll()).Returns(1);

            //when
            int secondRoll = _sut.GetSecondRoll();

            //then
            Assert.That(secondRoll, Is.EqualTo(1));
        }

        [Test]
        public void should_return_score_correctly()
        {
            //given
            _mockFrame.Setup(x => x.GetScore()).Returns(1);
            _mockFrame.Setup(x => x.GetFirstRoll()).Returns(1);
            _mockFrame.Setup(x => x.GetSecondRoll()).Returns(1);
            _sut.FirstRoll = 10;
            

            //when
            int score = _sut.GetScore();

            //then
            Assert.That(score, Is.EqualTo(13));
        }
    }
}
