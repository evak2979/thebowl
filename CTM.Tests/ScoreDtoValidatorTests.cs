﻿using System;
using CMT.Business.Concretes;
using CMT.Domain;
using NUnit.Framework;

namespace CTM.Unit
{
    [TestFixture]
    public class ScoreDtoValidatorTests
    {
        private ScoreDtoValidator _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new ScoreDtoValidator();
        }

        [TestCase("-")]
        [TestCase("X/")]
        [TestCase("X1")]
        [TestCase("/X")]
        [TestCase("1X")]
        [TestCase("-X")]
        [TestCase("X-")]
        [TestCase("9-/")]
        [TestCase("/")]
        [TestCase("9A")]
        [TestCase("9!")]
        [TestCase("9/")]
        [TestCase("/9")]
        public void should_throw_argument_exception_for_invalid_bonus_rolls(string bonusRolls)
        {
            //given + when + then
            var ex = Assert.Throws<ArgumentException>(() => _sut.ValidateScoreDto(new ScoreDto()
            {
                BonusRolls = bonusRolls
            }));
            Assert.That(ex.Message, Is.EqualTo("Invalid bonus rolls value"));
        }

        [TestCase("X/")]
        [TestCase("X1")]
        [TestCase("/X")]
        [TestCase("1X")]
        [TestCase("-X")]
        [TestCase("X-")]
        [TestCase("9-/")]
        [TestCase("9")]
        [TestCase("-")]
        [TestCase("/")]
        [TestCase("9A")]
        [TestCase("9!")]
        public void should_throw_argument_exception_for_an_invalid_frame(string frame)
        {
            //given + when + then
            var ex = Assert.Throws<ArgumentException>(() => _sut.ValidateScoreDto(new ScoreDto()
                {
                    Frames = new[] { "X", "7/", "9-", "X", "-8", "8/", "-6", "X", "X", $"{frame}" },
                    BonusRolls = "81"
                }));
            Assert.That(ex.Message, Is.EqualTo("Frame number 10 of the scorestring is erroneous."));
        }

                [Test]
        public void should_throw_an_argument_exception_for_a_frame_with_a_score_higher_than_ten()
        {
            //given + when + then
            var ex = Assert.Throws<ArgumentException>(() => _sut.ValidateScoreDto(new ScoreDto()
            {
                Frames = new[] { "X", "7/", "9-", "X", "-8", "8/", "-6", "X", "X", "99" },
                BonusRolls = "81"
            }));
            Assert.That(ex.Message, Is.EqualTo("Frame score cannot be higher than ten. Frame: 10"));
        }


        [Test]
        public void should_discover_ten_frames_in_a_score_string()
        {
            //given + when + then
            var ex = Assert.Throws<ArgumentException>(() => _sut.ValidateScoreDto(new ScoreDto()
                {
                    Frames = new[] { "X", "7/", "9-", "X", "-8", "8/", "-6", "X", "X" }
                }));
            Assert.That(ex.Message, Is.EqualTo("There should be ten frames in the score string. Number of frames found: 9"));
        }

        [Test]
        public void should_not_throw_an_exception_for_a_correct_string()
        {
            //given + when + then
            _sut.ValidateScoreDto(new ScoreDto()
            {
                Frames = new[] { "X", "7/", "9-", "X", "28", "8/", "-6", "X", "X", "X" }
            });
        }
    }
}
