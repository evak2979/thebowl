﻿using CMT.Business.Concretes;
using NUnit.Framework;

namespace CTM.Unit
{
    public class RollsFactoryTests
    {
        private RollsFactory _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new RollsFactory();
        }

        [TestCase("X")]
        [TestCase("x")]
        public void should_return_the_number_10_for_X(string input)
        {
            //given + when
            Rolls result = _sut.GetRolls(input);

            //then
            Assert.That(result.FirstRoll, Is.EqualTo(10));
        }

        [TestCase("XX")]
        [TestCase("xx")]
        public void should_populate_a_bonus_rolls_with_ten_in_case_of_double_strike(string input)
        {
            //given + when
            Rolls result = _sut.GetRolls(input);

            //then
            Assert.That(result.FirstRoll, Is.EqualTo(10));
            Assert.That(result.SecondRoll, Is.EqualTo(10));
        }

        [Test]
        public void should_return_the_number_0_for_dash()
        {
            //given + when
            Rolls result = _sut.GetRolls("-3");

            //then
            Assert.That(result.FirstRoll, Is.EqualTo(0));
            Assert.That(result.SecondRoll, Is.EqualTo(3));
        }

        [Test]
        public void should_return_the_remaining_number_for_a_spare()
        {
            //given + when
            Rolls result = _sut.GetRolls("3/");

            //then
            Assert.That(result.FirstRoll, Is.EqualTo(3));
            Assert.That(result.SecondRoll, Is.EqualTo(7));
        }

        [Test]
        public void should_return_the_number_if_the_string_is_a_number()
        {
            //given + when
            Rolls result = _sut.GetRolls("12");

            //then
            Assert.That(result.FirstRoll, Is.EqualTo(1));
            Assert.That(result.SecondRoll, Is.EqualTo(2));
        }


        [Test]
        public void should_return_correct_roll_for_a_strike()
        {
            //given + when
            Rolls result = _sut.GetRolls("X");

            //then
            Assert.That(result.FirstRoll, Is.EqualTo(10));
        }

        [Test]
        public void should_return_an_empty_roll_for_a_zero_bonus_roll()
        {
            //given + when
            Rolls result = _sut.GetRolls(string.Empty);

            //then
            Assert.That(result.FirstRoll, Is.EqualTo(0));
            Assert.That(result.SecondRoll, Is.EqualTo(0));
        }

        [Test]
        public void should_return_a_roll_with_only_firstroll_for_a_single_bonus_roll()
        {
            //given + when
            Rolls result = _sut.GetRolls("1");

            //then
            Assert.That(result.FirstRoll, Is.EqualTo(1));
        }

        [Test]
        public void should_return_a_normal_roll_with_both_values_populated()
        {
            //given + when
            Rolls result = _sut.GetRolls("12");

            //then
            Assert.That(result.FirstRoll, Is.EqualTo(1));
            Assert.That(result.SecondRoll, Is.EqualTo(2));
        }
    }
}
