﻿using CMT.Business.Concretes;
using CMT.Business.Interfaces;
using CMT.Domain;
using Moq;
using NUnit.Framework;

namespace CTM.Unit
{
    [TestFixture]
    public class ScoreCalculatorTests
    {
        private ScoreCalculator _sut;
        private Mock<IScoreDtoValidator> _mockScoreStringValidator;
        private Mock<IScoreDtoBuilder> _mockScoreStringParser;
        private Mock<IFrameFactory> _mockFrameFactory;
        private Mock<IFrame> _mockFrame;
        private ScoreDto _scoreDtoStub;

        [SetUp]
        public void Setup()
        {
            _mockScoreStringValidator = new Mock<IScoreDtoValidator>();
            _mockScoreStringParser = new Mock<IScoreDtoBuilder>();
            _mockFrameFactory = new Mock<IFrameFactory>();
            _mockFrame = new Mock<IFrame>();
            _scoreDtoStub = new ScoreDto();

            _sut = new ScoreCalculator(_mockScoreStringValidator.Object,
                _mockScoreStringParser.Object,
                _mockFrameFactory.Object);
        }

        [Test]
        public void should_calculate_score_properly()
        {
            //given
            string scoreString = "ascorestring";
            _mockFrame.Setup(x => x.GetScore()).Returns(100);
            _mockScoreStringParser.Setup(x => x.GetScoreDto(scoreString)).Returns(_scoreDtoStub);
            _mockFrameFactory.Setup(x => x.GetFrame(_scoreDtoStub)).Returns(_mockFrame.Object);

            //when
            int score = _sut.CalculateScore(scoreString);

            //then
            Assert.That(score, Is.EqualTo(100));
            _mockFrame.Verify(x=>x.GetScore());
            _mockScoreStringParser.Verify(x => x.GetScoreDto(scoreString));
            _mockFrameFactory.Verify(x => x.GetFrame(_scoreDtoStub));
        }
    }
}

