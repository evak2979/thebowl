﻿using CMT.Business.Concretes;
using CMT.Business.Interfaces;
using CMT.Domain;
using Moq;
using NUnit.Framework;

namespace CTM.Unit
{
    [TestFixture]
    public class DecoratorFrameFactoryTests
    {
        private DecoratedFrameFactory _sut;
        private Mock<IRollsFactory> _mockRollsFactory;
        

        [SetUp]
        public void Setup()
        {
            _mockRollsFactory = new Mock<IRollsFactory>();
            _mockRollsFactory.Setup(x => x.GetRolls("12")).
            Returns(new Rolls()
            {
                FirstRoll = 1,
                SecondRoll = 2
            });

            _sut = new DecoratedFrameFactory(_mockRollsFactory.Object);
        }

        [Test]
        public void should_create_a_bonus_frame_properly()
        {
            //given
            ScoreDto scoreDto = new ScoreDto()
            {
                BonusRolls = "12",
                Frames = new string[0]
            };

            //when
            IFrame frame = _sut.GetFrame(scoreDto);

            //then
            Assert.True(frame is BonusFrame);
            Assert.That(frame.GetFirstRoll(), Is.EqualTo(1));
            Assert.That(frame.GetSecondRoll(), Is.EqualTo(2));
        }

        [Test]
        public void should_create_a_strike_frame_properly()
        {
            //given
            _mockRollsFactory.Setup(x => x.GetRolls("X")).
            Returns(new Rolls()
            {
                FirstRoll = 10,
            });

            ScoreDto scoreDto = new ScoreDto()
            {
                BonusRolls = "12",
                Frames = new []{"X"}
            };

            //when
            IFrame frame = _sut.GetFrame(scoreDto);

            //then
            Assert.True(frame is StrikeFrame);
            Assert.That(frame.GetFirstRoll(), Is.EqualTo(10));
            Assert.That(frame.GetSecondRoll(), Is.EqualTo(1));
        }

        [Test]
        public void should_create_a_spare_frame_properly()
        {
            //given
            _mockRollsFactory.Setup(x => x.GetRolls("3/")).
            Returns(new Rolls()
            {
                FirstRoll = 3,
                SecondRoll = 7
            });

            ScoreDto scoreDto = new ScoreDto()
            {
                BonusRolls = "12",
                Frames = new[] { "3/" }
            };

            //when
            IFrame frame = _sut.GetFrame(scoreDto);

            //then
            Assert.True(frame is SpareFrame);
            Assert.That(frame.GetFirstRoll(), Is.EqualTo(3));
            Assert.That(frame.GetSecondRoll(), Is.EqualTo(7));
        }

        [Test]
        public void should_create_a_normal_frame_properly()
        {
            //given
            _mockRollsFactory.Setup(x => x.GetRolls("34")).
            Returns(new Rolls()
            {
                FirstRoll = 3,
                SecondRoll = 4
            });

            ScoreDto scoreDto = new ScoreDto()
            {
                BonusRolls = "12",
                Frames = new[] { "34" }
            };

            //when
            IFrame frame = _sut.GetFrame(scoreDto);

            //then
            Assert.True(frame is Frame);
            Assert.False(frame is SpareFrame);
            Assert.False(frame is StrikeFrame);
            Assert.False(frame is BonusFrame);
            Assert.That(frame.GetFirstRoll(), Is.EqualTo(3));
            Assert.That(frame.GetSecondRoll(), Is.EqualTo(4));
        }
    }
}
