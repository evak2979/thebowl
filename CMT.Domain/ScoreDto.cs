﻿using System.Collections;
using System.Collections.Generic;

namespace CMT.Domain
{
    public class ScoreDto
    {
        public string BonusRolls { get; set; }
        public string[] Frames { get; set; }
    }
}
