﻿using System;
using System.Linq;
using CMT.Business.Concretes;
using CMT.Business.Interfaces;

namespace CTM.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            if (IsValidArgs(args))
            {
                string input = args[0];

                IScoreCalculator calculator = new ScoreCalculator(new ScoreDtoValidator(), new ScoreDtoBuilder(),
                    new DecoratedFrameFactory(new RollsFactory()));
                try
                {
                    Console.WriteLine($"Total Score:{calculator.CalculateScore(input)}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            Console.WriteLine("Press any key to exit the program.");
            Console.ReadLine();
        }

        private static bool IsValidArgs(string[] args)
        {
            if (IsEmptyArgs(args))
            {
                Console.WriteLine("Please pass the input string as an argument");
                return false;
            }

            if (IsInvalidArgs(args))
            {
                Console.WriteLine("This console application accepts a single argument");
                return false;
            }

            return true;
        }

        private static bool IsInvalidArgs(string[] args)
        {
            return args.Length != 1;
        }

        static bool IsEmptyArgs(string[] args)
        {
            return args == null || !args.Any();
        }
    }
}
