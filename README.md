Steps:
A string is passed to the application, and from there to the ScoreCalculator's CalculateScore.
The CalculateScore method calls the IScoreDtoBuilder's GetScoreDto method to create a strongly typed ScoreDto, that holds the bonus roll frame in one property, and an array of the rest of the frames in the other.

The resulting scoreDto, is passed to a ScoreValidatorDto object, which verifies that both regular frames and bonus frames have an acceptable format. The reason I took this approach, was the hardness of coming up with a readable regularexpression that would parse the entire input string at one go. By breaking it down in frames, I can instead validate each frame as well as give elegant error report of which frame (if any) has any issues and what sort of issues those are.

If validation fails, an exception with a detailed message is thrown which is handled gracefully by the UI. If it succeeds ->

the scoreDto object is passed to an IFrame factory. My implementation for the IFrame factory is the decorator pattern. I have created four different frame types (frame, which acts as base, spare, strike and bonus). Each of the children shares the FirstRoll and SecondRoll properties of the parent, and appropriately overrides the GetFirstRoll(), GetSecondRoll(), and GetScore() properties, while also keeping the next frame as a variable for reference. To create this decoration, the decoratorfactory reverses the scoreDto's Frames string array. It then passes each frame into the RollsFactory which produces a self aware Rolls business object (it knows whether it's a Strike, a Spare, or a Normal by evaluating on its properties). Those objects are used to create the appropriate implementation of each of the frames classes. The Factory adds the first entry as the BonusFrame, passing a null for its next frame reference (since the bonusframe is the very last frame). Proceeding through the reversed array, it then adds each previous frame with its successor injected in its constructor. (The Bonus frame is injected to the tenth, which is injected to the ninth, etc.). When calling GetScore() on the final frame, the decorator pattern kicks in to help us get the total score.


The commit history demonstrates the approach, albeit with large parts of refactoring after passing tests. 

As a final step I have added integration end-to-end tests that check the ScoreCalculator against the example string provided in the exercise.


Finally:
An obvious alternative to my solution would be uncle bob's Bowling Kata:
Though I am aware of that option, as one of the first katas I played with some time ago, I felt it would hardly demonstrate desining a problem following a proper OOP/TDD approach. It would also hardly demonstrate my ability to think or design, by chewing out a ready made solution! So I decided to treat this like a proper application, where one had to think TDD/Design Patterns first, and come up with an elegant solution of many decoupled components working with one another. 

Thanks for reviewing my solution!